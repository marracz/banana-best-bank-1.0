package com.onegini.bananabestbank.balance.infrastructure.rest;

import com.onegini.bananabestbank.balance.BalanceReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class BalanceController {

    private final BalanceReader balanceReader;

    @Autowired
    public BalanceController(BalanceReader balanceReader) {
        this.balanceReader = balanceReader;
    }

    @GetMapping(value = "/balance/user/{userId}")
    public BalanceResponse getBalanceForUser(@PathVariable("userId") UUID userId) {
        return new BalanceResponse(balanceReader.read(userId).getValue());
    }
}
