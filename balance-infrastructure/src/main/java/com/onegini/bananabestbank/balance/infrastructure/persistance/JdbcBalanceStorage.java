package com.onegini.bananabestbank.balance.infrastructure.persistance;

import com.onegini.bananabestbank.balance.Balance;
import com.onegini.bananabestbank.balance.BalanceStorage;
import com.onegini.bananabestbank.transaction.common.BalanceUpdater;
import com.onegini.bananabestbank.transaction.common.UpdatableBalance;
import com.onegini.bananabestbank.withdrawal.BalanceChecker;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.UUID;
import java.util.function.Consumer;

@Transactional
public class JdbcBalanceStorage implements BalanceStorage, BalanceUpdater, BalanceChecker {

    private final BalanceRepository balanceRepository;

    public JdbcBalanceStorage(BalanceRepository balanceRepository) {
        this.balanceRepository = balanceRepository;
    }

    @Override
    public Balance fetchBalance(UUID userId) {
        return balanceRepository.findByUserId(userId)
                .map(balance -> new Balance(balance.getValue()))
                .orElseThrow(() -> new BalanceNotFoundException("Balance for user " + userId + " not found"));
    }

    @Override
    public void update(Consumer<UpdatableBalance> operation, UUID userId) {
        balanceRepository.findByUserId(userId).ifPresentOrElse(
                balanceEntity -> {
                    operation.accept(balanceEntity);
                    balanceRepository.save(balanceEntity);
                },
                () -> {
                    throw new BalanceMissingException("Balance for user ".concat(userId.toString()).concat(" does not exist. User record corrupted"));
                });
    }

    @Override
    public boolean hasEnoughMoney(BigDecimal value, UUID userId) {
        return balanceRepository.findByUserId(userId)
                .map(BalanceEntity::getValue)
                .filter(v -> v.compareTo(value) >= 0)
                .isPresent();
    }
}
