package com.onegini.bananabestbank.balance.infrastructure.persistance;

import com.onegini.bananabestbank.transaction.common.UpdatableBalance;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.UUID;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class BalanceEntity implements UpdatableBalance {

    @Id
    private UUID id;

    private BigDecimal value;

    private UUID userId;
}
