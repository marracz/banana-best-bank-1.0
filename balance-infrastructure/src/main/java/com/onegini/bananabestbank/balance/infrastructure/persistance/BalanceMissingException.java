package com.onegini.bananabestbank.balance.infrastructure.persistance;

public class BalanceMissingException extends RuntimeException {

    public BalanceMissingException(String msg) {
        super(msg);
    }
}
