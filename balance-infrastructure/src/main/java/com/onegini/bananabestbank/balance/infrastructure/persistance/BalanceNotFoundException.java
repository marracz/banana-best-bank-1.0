package com.onegini.bananabestbank.balance.infrastructure.persistance;

public class BalanceNotFoundException extends RuntimeException {

    public BalanceNotFoundException(String message) {
        super(message);
    }
}
