package com.onegini.bananabestbank.balance.infrastructure;

import com.onegini.bananabestbank.balance.BalanceReader;
import com.onegini.bananabestbank.balance.BalanceReaderService;
import com.onegini.bananabestbank.balance.BalanceStorage;
import com.onegini.bananabestbank.balance.infrastructure.persistance.BalanceRepository;
import com.onegini.bananabestbank.balance.infrastructure.persistance.JdbcBalanceStorage;
import com.onegini.bananabestbank.transaction.common.BalanceUpdater;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BalanceConfiguration {

    @Bean
    BalanceStorage balanceStorage(BalanceRepository balanceRepository) {
        return new JdbcBalanceStorage(balanceRepository);
    }

//    @Bean
//    BalanceUpdater balanceUpdater(BalanceRepository balanceRepository) {
//        return new JdbcBalanceStorage(balanceRepository);
//    }

    @Bean
    BalanceReader balanceReaderService(BalanceStorage balanceStorage) {
        return new BalanceReaderService(balanceStorage);
    }
}
