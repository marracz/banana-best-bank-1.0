package com.onegini.bananabestbank.balance.infrastructure.persistance;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.UUID;

public interface BalanceRepository extends CrudRepository<BalanceEntity, UUID> {

    Optional<BalanceEntity> findByUserId(UUID id);
}
