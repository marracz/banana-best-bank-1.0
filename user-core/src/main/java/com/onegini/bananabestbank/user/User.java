package com.onegini.bananabestbank.user;

import lombok.AllArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
public class User {

    private UUID id;
}
