package com.onegini.bananabestbank.user;

import java.util.Optional;
import java.util.UUID;

public class UserService {

    private final UserStorage userStorage;

    public UserService(UserStorage userStorage) {
        this.userStorage = userStorage;
    }

    public Optional<User> findById(UUID id) {
        return userStorage.findById(id);
    }
}
