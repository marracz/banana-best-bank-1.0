package com.onegini.bananabestbank.user;

import java.util.Optional;
import java.util.UUID;

public interface UserStorage {

    Optional<User> findById(UUID id);
}
