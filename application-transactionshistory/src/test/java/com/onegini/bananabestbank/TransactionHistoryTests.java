package com.onegini.bananabestbank;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.onegini.bananabestbank.transactionshistory.infrastructure.rest.TransactionEntry;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@AutoConfigureMockMvc
@AutoConfigureWebMvc
@SpringBootTest(properties = {
        "spring.jpa.hibernate.ddl-auto=create",
        "spring.jpa.properties.hibernate.hbm2ddl.import_files=schema.sql,import.sql",
        "spring.jpa.show-sql=true"
})
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class TransactionHistoryTests {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldReturnTransactionHistory() throws Exception {

        // given
        UUID userId = UUID.fromString("e592a2aa-da5e-0b46-aa27-2a4a51faab18");

        // when
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/history/user/".concat(userId.toString())))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        System.out.println(mvcResult.getResponse().getContentAsString());

        // then
        List<TransactionEntry> response = MAPPER.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<>() {});
        Assertions.assertThat(response).hasSize(3);
        assertThat(response.get(0).getType()).isEqualTo("increase");
        assertThat(response.get(0).getValue()).isEqualTo(new BigDecimal("1000.00"));
        assertThat(response.get(1).getType()).isEqualTo("decrease");
        assertThat(response.get(1).getValue()).isEqualTo(new BigDecimal("300.00"));
        assertThat(response.get(2).getType()).isEqualTo("increase");
        assertThat(response.get(2).getValue()).isEqualTo(new BigDecimal("100.00"));
    }

}
