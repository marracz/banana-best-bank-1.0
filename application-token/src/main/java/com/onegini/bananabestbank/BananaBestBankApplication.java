package com.onegini.bananabestbank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BananaBestBankApplication {

	public static void main(String[] args) {
		SpringApplication.run(BananaBestBankApplication.class, args);
	}

}
