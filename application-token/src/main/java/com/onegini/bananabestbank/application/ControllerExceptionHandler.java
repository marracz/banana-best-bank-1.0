package com.onegini.bananabestbank.application;

import com.onegini.bananabestbank.transaction.common.UserNotFoundException;
import com.onegini.bananabestbank.withdrawal.NotEnoughMoneyException;
import com.onegini.bananabestbank.withdrawal.TokenNotValidException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@ControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {UserNotFoundException.class})
    protected ResponseEntity<Object> handleNotFound(RuntimeException ex, WebRequest request) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON_VALUE);
        return handleExceptionInternal(ex, Map.of("error", ex.getMessage()),
                httpHeaders, HttpStatus.NOT_FOUND, request);
    }


    @ExceptionHandler(value = {NotEnoughMoneyException.class, TokenNotValidException.class})
    protected ResponseEntity<Object> handleBadRequest(RuntimeException ex, WebRequest request) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON_VALUE);
        return handleExceptionInternal(ex, Map.of("error", ex.getMessage()),
                httpHeaders, HttpStatus.BAD_REQUEST, request);
    }
}
