package com.onegini.bananabestbank.token;

import java.util.UUID;

public class TokenService {

    private final TokenStorage tokenStorage;

    public TokenService(TokenStorage tokenStorage) {
        this.tokenStorage = tokenStorage;
    }

    public Token create(UUID userId) {
        final Token token = new Token(UUID.randomUUID().toString().substring(0, 6));
        tokenStorage.saveToken(token, userId);
        return token;
    }
}
