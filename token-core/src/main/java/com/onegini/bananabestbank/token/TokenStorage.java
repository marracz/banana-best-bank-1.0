package com.onegini.bananabestbank.token;

import java.util.UUID;

public interface TokenStorage {

    void saveToken(Token token, UUID userId);
}
