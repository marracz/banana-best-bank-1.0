package com.onegini.bananabestbank;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.onegini.bananabestbank.balance.infrastructure.persistance.BalanceEntity;
import com.onegini.bananabestbank.balance.infrastructure.persistance.BalanceRepository;
import com.onegini.bananabestbank.token.infrastructure.persistance.TokenEntity;
import com.onegini.bananabestbank.token.infrastructure.persistance.TokenRepository;
import com.onegini.bananabestbank.transactionshistory.TransactionStorageProvider;
import com.onegini.bananabestbank.transactionshistory.infrastructure.rest.TransactionEntry;
import com.onegini.bananabestbank.user.infrastructure.UserEntity;
import com.onegini.bananabestbank.user.infrastructure.UserRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@AutoConfigureMockMvc
@AutoConfigureWebMvc
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class TransactionHistoryTests {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private BalanceRepository balanceRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TokenRepository tokenRepository;

    @Autowired
    private TransactionStorageProvider transactionRepository;

    @BeforeEach
    void setUp() {
        userRepository.deleteAll();
        tokenRepository.deleteAll();
        balanceRepository.deleteAll();
        transactionRepository.deleteAll();
    }

    @Test
    public void shouldReturnTransactionHistory() throws Exception {

        // given
        UUID userId = UUID.randomUUID();
        userRepository.save(new UserEntity(userId));
        balanceRepository.save(BalanceEntity.builder()
                .id(UUID.randomUUID())
                .userId(userId)
                .value(new BigDecimal(0))
                .build());
        mockMvc.perform(MockMvcRequestBuilders.post("/balance/user/".concat(userId.toString().concat("/increase")))
                .content("{\"value\": 1000}").
                        contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
        tokenRepository.save(new TokenEntity(UUID.randomUUID(), "up7kj8", userId));
        mockMvc.perform(MockMvcRequestBuilders.post("/balance/user/".concat(userId.toString().concat("/decrease")))
                .content("{\"value\": 300, \"token\": \"up7kj8\"}").
                        contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
        mockMvc.perform(MockMvcRequestBuilders.post("/balance/user/".concat(userId.toString().concat("/increase")))
                .content("{\"value\": 100}").
                        contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        // when
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/history/user/".concat(userId.toString())))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        // then
        List<TransactionEntry> response = MAPPER.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<List<TransactionEntry>>(){});
        Assertions.assertThat(response).hasSize(3);
        assertThat(response.get(0).getType()).isEqualTo("increase");
        assertThat(response.get(0).getValue()).isEqualTo(new BigDecimal("1000.00"));
        assertThat(response.get(1).getType()).isEqualTo("decrease");
        assertThat(response.get(1).getValue()).isEqualTo(new BigDecimal("300.00"));
        assertThat(response.get(2).getType()).isEqualTo("increase");
        assertThat(response.get(2).getValue()).isEqualTo(new BigDecimal("100.00"));
    }

}
