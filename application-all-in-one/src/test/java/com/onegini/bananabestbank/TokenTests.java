package com.onegini.bananabestbank;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.onegini.bananabestbank.token.infrastructure.rest.TokenResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@AutoConfigureWebMvc
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class TokenTests {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldCreateToken() throws Exception {

        // given
        UUID userId = UUID.randomUUID();

        // when
        final MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/token/user/".concat(userId.toString())))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        // then
        TokenResponse response = MAPPER.readValue(mvcResult.getResponse().getContentAsString(), TokenResponse.class);
        assertThat(response.getToken()).hasSize(6);
    }

}
