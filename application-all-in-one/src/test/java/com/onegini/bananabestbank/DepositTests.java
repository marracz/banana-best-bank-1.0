package com.onegini.bananabestbank;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.onegini.bananabestbank.balance.infrastructure.persistance.BalanceEntity;
import com.onegini.bananabestbank.balance.infrastructure.persistance.BalanceRepository;
import com.onegini.bananabestbank.balance.infrastructure.rest.BalanceResponse;
import com.onegini.bananabestbank.user.infrastructure.UserEntity;
import com.onegini.bananabestbank.user.infrastructure.UserRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@AutoConfigureWebMvc
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class DepositTests {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private BalanceRepository balanceRepository;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void shouldIncreaseBalanceWhenNewMoneyDeposited() throws Exception {

        // given
        UUID userId = UUID.randomUUID();
        userRepository.save(new UserEntity(userId));
        balanceRepository.save(BalanceEntity.builder()
                .id(UUID.randomUUID())
                .userId(userId)
                .value(new BigDecimal(400))
                .build());

        // when
        mockMvc.perform(MockMvcRequestBuilders.post("/balance/user/".concat(userId.toString().concat("/increase")))
                .content("{\"value\": 1500}").
                        contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/balance/user/".concat(userId.toString())))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        // then
        BalanceResponse response = MAPPER.readValue(mvcResult.getResponse().getContentAsString(), BalanceResponse.class);
        Assertions.assertThat(response.getValue()).isEqualTo(new BigDecimal("1900.00"));
    }

    @Test
    public void shouldThrowUserNotFoundExceptionIfUserMissing() throws Exception {

        // given
        UUID userId = UUID.randomUUID();

        // when
        mockMvc.perform(MockMvcRequestBuilders.post("/balance/user/".concat(userId.toString().concat("/increase")))
                .content("{\"value\": 1500}").
                        contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andReturn();
    }

    @Test
    public void shouldThrowIllegalStateExceptionIfBalanceMissing() throws Exception {

        // given
        UUID userId = UUID.randomUUID();
        userRepository.save(new UserEntity(userId));

        // when
        mockMvc.perform(MockMvcRequestBuilders.post("/balance/user/".concat(userId.toString().concat("/increase")))
                .content("{\"value\": 1500}").
                        contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();
    }
}
