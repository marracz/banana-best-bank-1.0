package com.onegini.bananabestbank.deposit;

import java.util.List;
import java.util.UUID;

public interface DepositStorage {

    void applyDeposit(Deposit deposit, UUID userId);

    List<Deposit> getDepositHistory(UUID userId);
}
