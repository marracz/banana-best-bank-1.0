package com.onegini.bananabestbank.deposit;

import com.onegini.bananabestbank.transaction.common.BalanceUpdater;
import com.onegini.bananabestbank.transaction.common.UpdatableBalance;
import com.onegini.bananabestbank.transaction.common.UserNotFoundException;
import com.onegini.bananabestbank.transaction.common.UserProvider;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

public class DepositService {

    private final DepositStorage depositStorage;
    private final BalanceUpdater balanceUpdater;
    private final UserProvider userProvider;

    public DepositService(DepositStorage depositStorage, BalanceUpdater balanceUpdater, UserProvider userProvider) {
        this.depositStorage = depositStorage;
        this.balanceUpdater = balanceUpdater;
        this.userProvider = userProvider;
    }

    public void makeDeposit(Deposit deposit, UUID userId) {
        validateUserExists(userId);
        depositStorage.applyDeposit(deposit, userId);
        updateBalance(deposit.getValue(), userId);
    }

    private void validateUserExists(UUID userId) {
        if (!userProvider.exists(userId)) {
            throw new UserNotFoundException("User with id ".concat(userId.toString()).concat(" was not found"));
        }
    }

    private void updateBalance(BigDecimal value, UUID userId) {
        balanceUpdater.update(increase(value), userId);
    }

    private Consumer<UpdatableBalance> increase(BigDecimal value) {
        return balance -> balance.setValue(value.add(balance.getValue()));
    }

    public List<Deposit> getDepositHistory(UUID userId) {
        return depositStorage.getDepositHistory(userId);
    }
}
