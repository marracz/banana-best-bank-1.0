package com.onegini.bananabestbank.deposit;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class Deposit {

    private BigDecimal value;
    private LocalDateTime dateTime;
}
