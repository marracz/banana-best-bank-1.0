package com.onegini.bananabestbank.withdrawal.infrastructure.persistance;

import org.springframework.data.repository.CrudRepository;

import java.util.UUID;
import java.util.stream.Stream;

public interface WithdrawnRepository extends CrudRepository<WithdrawnEntity, UUID> {

    Stream<WithdrawnEntity> findByUserId(UUID userId);
}
