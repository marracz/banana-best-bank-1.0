package com.onegini.bananabestbank.withdrawal.infrastructure.persistance;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class WithdrawnEntity {

    @Id
    private UUID id;

    private BigDecimal value;

    private UUID userId;

    private LocalDateTime dateTime;
}
