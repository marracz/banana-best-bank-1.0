package com.onegini.bananabestbank.withdrawal.infrastructure.persistance;

import com.onegini.bananabestbank.withdrawal.Withdrawn;
import com.onegini.bananabestbank.withdrawal.WithdrawnStorage;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Transactional
public class JdbcWithdrawnStorage implements WithdrawnStorage {

    private final WithdrawnRepository withdrawnRepository;

    public JdbcWithdrawnStorage(WithdrawnRepository withdrawnRepository) {
        this.withdrawnRepository = withdrawnRepository;
    }

    @Override
    public void applyWithdrawn(Withdrawn withdrawn, UUID userId) {
        withdrawnRepository.save(new WithdrawnEntity(UUID.randomUUID(), withdrawn.getValue(), userId, withdrawn.getDateTime()));
    }

    @Override
    public List<Withdrawn> getWithdrawnHistory(UUID userId) {
        return withdrawnRepository.findByUserId(userId)
                .map(withdrawnEntity -> new Withdrawn(withdrawnEntity.getValue(), null, withdrawnEntity.getDateTime()))
                .collect(Collectors.toList());
    }
}
