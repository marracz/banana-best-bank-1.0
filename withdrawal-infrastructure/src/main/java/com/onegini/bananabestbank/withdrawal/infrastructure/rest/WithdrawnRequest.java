package com.onegini.bananabestbank.withdrawal.infrastructure.rest;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class WithdrawnRequest {

    private BigDecimal value;
    private String token;
}
