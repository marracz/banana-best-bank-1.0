package com.onegini.bananabestbank.withdrawal.infrastructure.rest;

import com.onegini.bananabestbank.withdrawal.WithdrawalService;
import com.onegini.bananabestbank.withdrawal.Withdrawn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
public class WithdrawalController {

    private final WithdrawalService withdrawalService;

    @Autowired
    public WithdrawalController(WithdrawalService withdrawalService) {
        this.withdrawalService = withdrawalService;
    }

    @PostMapping(value = "/balance/user/{userId}/decrease", consumes = APPLICATION_JSON_VALUE)
    public void withdrawn(@RequestBody WithdrawnRequest withdrawnRequest, @PathVariable("userId") UUID userId) {
        withdrawalService.makeWithdrawn(new Withdrawn(withdrawnRequest.getValue(), withdrawnRequest.getToken(), LocalDateTime.now()), userId);
    }
}
