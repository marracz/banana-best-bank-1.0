package com.onegini.bananabestbank.withdrawal.infrastructure;

import com.onegini.bananabestbank.transaction.common.BalanceUpdater;
import com.onegini.bananabestbank.transaction.common.UserProvider;
import com.onegini.bananabestbank.withdrawal.BalanceChecker;
import com.onegini.bananabestbank.withdrawal.TokenValidator;
import com.onegini.bananabestbank.withdrawal.WithdrawalService;
import com.onegini.bananabestbank.withdrawal.WithdrawnStorage;
import com.onegini.bananabestbank.withdrawal.infrastructure.persistance.JdbcWithdrawnStorage;
import com.onegini.bananabestbank.withdrawal.infrastructure.persistance.WithdrawnRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WithdrawalConfiguration {

    @Bean
    public WithdrawalService withdrawalService(WithdrawnStorage withdrawnStorage, BalanceChecker balanceChecker,
                                               BalanceUpdater balanceUpdater, UserProvider userProvider, TokenValidator tokenValidator) {
        return new WithdrawalService(withdrawnStorage, balanceUpdater, balanceChecker, userProvider, tokenValidator);
    }

    @Bean
    public WithdrawnStorage withdrawnStorage(WithdrawnRepository withdrawnRepository) {
        return new JdbcWithdrawnStorage(withdrawnRepository);
    }
}
