package com.onegini.bananabestbank.transactionshistory;

import java.util.List;
import java.util.UUID;

public interface TransactionStorageProvider {

    List<Transaction> fetchHistory(UUID userId);

    void deleteAll();
}
