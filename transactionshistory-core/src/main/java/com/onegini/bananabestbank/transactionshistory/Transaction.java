package com.onegini.bananabestbank.transactionshistory;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@AllArgsConstructor
@Data
public class Transaction {

    private Type type;
    private BigDecimal value;
    private LocalDateTime dateTime;
}
