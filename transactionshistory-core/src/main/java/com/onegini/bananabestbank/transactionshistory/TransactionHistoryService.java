package com.onegini.bananabestbank.transactionshistory;

import java.util.List;
import java.util.UUID;

public class TransactionHistoryService {

    private final TransactionStorageProvider transactionStorageProvider;

    public TransactionHistoryService(TransactionStorageProvider transactionStorageProvider) {
        this.transactionStorageProvider = transactionStorageProvider;
    }

    public List<Transaction> getTransactionHistory(UUID userId) {
        return transactionStorageProvider.fetchHistory(userId);
    }
}
