package com.onegini.bananabestbank.transactionshistory;

public enum Type {

    INCREASE,
    DECREASE
}
