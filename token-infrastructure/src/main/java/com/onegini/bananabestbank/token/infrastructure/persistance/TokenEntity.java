package com.onegini.bananabestbank.token.infrastructure.persistance;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class TokenEntity {

    @Id
    private UUID id;

    private String token;

    private UUID userId;
}
