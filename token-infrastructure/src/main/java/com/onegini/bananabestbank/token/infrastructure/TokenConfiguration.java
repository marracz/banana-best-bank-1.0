package com.onegini.bananabestbank.token.infrastructure;

import com.onegini.bananabestbank.token.TokenService;
import com.onegini.bananabestbank.token.TokenStorage;
import com.onegini.bananabestbank.token.infrastructure.persistance.JdbcTokenStorage;
import com.onegini.bananabestbank.token.infrastructure.persistance.TokenRepository;
import com.onegini.bananabestbank.withdrawal.TokenValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TokenConfiguration {

    @Bean
    public TokenService tokenService(TokenStorage tokenStorage) {
        return new TokenService(tokenStorage);
    }

    @Bean
    public TokenStorage tokenStorage(TokenRepository tokenRepository) {
        return new JdbcTokenStorage(tokenRepository);
    }

    @Bean
    public TokenValidator tokenValidator(TokenRepository tokenRepository) {
        return new JdbcTokenStorage(tokenRepository);
    }
}
