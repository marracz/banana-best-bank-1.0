package com.onegini.bananabestbank.token.infrastructure.rest;

import com.onegini.bananabestbank.token.Token;
import com.onegini.bananabestbank.token.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class TokenController {

    private final TokenService tokenService;

    @Autowired
    public TokenController(TokenService tokenService) {
        this.tokenService = tokenService;
    }

    @PostMapping(value = "/token/user/{userId}")
    public TokenResponse create(@PathVariable("userId") UUID userId) {
        final Token token = tokenService.create(userId);
        return new TokenResponse(token.getValue());
    }
}
