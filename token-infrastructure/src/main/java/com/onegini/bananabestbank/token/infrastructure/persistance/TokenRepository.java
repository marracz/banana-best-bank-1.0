package com.onegini.bananabestbank.token.infrastructure.persistance;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.UUID;

public interface TokenRepository extends CrudRepository<TokenEntity, UUID> {
    Optional<TokenEntity> findByTokenAndUserId(String token, UUID userId);
}
