package com.onegini.bananabestbank.token.infrastructure.persistance;

import com.onegini.bananabestbank.token.Token;
import com.onegini.bananabestbank.token.TokenStorage;
import com.onegini.bananabestbank.withdrawal.TokenValidator;

import java.util.UUID;

public class JdbcTokenStorage implements TokenStorage, TokenValidator {

    private final TokenRepository tokenRepository;

    public JdbcTokenStorage(TokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    @Override
    public void saveToken(Token token, UUID userId) {
        tokenRepository.save(new TokenEntity(UUID.randomUUID(), token.getValue(), userId));
    }

    @Override
    public boolean isValid(String token, UUID userId) {
        return tokenRepository.findByTokenAndUserId(token, userId).isPresent();
    }
}
