package com.onegini.bananabestbank;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.onegini.bananabestbank.balance.infrastructure.persistance.BalanceEntity;
import com.onegini.bananabestbank.balance.infrastructure.persistance.BalanceRepository;
import com.onegini.bananabestbank.balance.infrastructure.rest.BalanceResponse;
import com.onegini.bananabestbank.token.infrastructure.persistance.TokenEntity;
import com.onegini.bananabestbank.token.infrastructure.persistance.TokenRepository;
import com.onegini.bananabestbank.user.infrastructure.UserEntity;
import com.onegini.bananabestbank.user.infrastructure.UserRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.util.UUID;

@AutoConfigureMockMvc
@AutoConfigureWebMvc
@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class WithdrawalTests {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private BalanceRepository balanceRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TokenRepository tokenRepository;

    @Test
    public void shouldDecreaseBalanceWhenMoneyWithdrawn() throws Exception {

        // given
        UUID userId = UUID.randomUUID();
        userRepository.save(new UserEntity(userId));
        tokenRepository.save(new TokenEntity(UUID.randomUUID(), "ds8kal", userId));
        balanceRepository.save(BalanceEntity.builder()
                .id(UUID.randomUUID())
                .userId(userId)
                .value(new BigDecimal(400))
                .build());

        // when
        mockMvc.perform(MockMvcRequestBuilders.post("/balance/user/".concat(userId.toString().concat("/decrease")))
                .content("{\"value\": 100, \"token\": \"ds8kal\"}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/balance/user/".concat(userId.toString())))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        // then
        BalanceResponse response = MAPPER.readValue(mvcResult.getResponse().getContentAsString(), BalanceResponse.class);
        Assertions.assertThat(response.getValue()).isEqualTo(new BigDecimal("300.00"));
    }

    @Test
    public void shouldThrowUserNotFoundExceptionIfUserMissing() throws Exception {

        // given
        UUID userId = UUID.randomUUID();

        // when
        mockMvc.perform(MockMvcRequestBuilders.post("/balance/user/".concat(userId.toString().concat("/decrease")))
                .content("{\"value\": 1500}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andReturn();
    }

    @Test
    public void shouldThrowTokenNotValidException() throws Exception {

        // given
        UUID userId = UUID.randomUUID();
        userRepository.save(new UserEntity(userId));
        tokenRepository.save(new TokenEntity(UUID.randomUUID(), "p9ewjl", userId));

        // when
        mockMvc.perform(MockMvcRequestBuilders.post("/balance/user/".concat(userId.toString().concat("/decrease")))
                .content("{\"value\": 1500, \"token\": \"3a81de\"}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();
    }

    @Test
    public void shouldThrowNotEnoughMoneyExceptionIfNotEnoughBalance() throws Exception {

        // given
        UUID userId = UUID.randomUUID();
        userRepository.save(new UserEntity(userId));
        tokenRepository.save(new TokenEntity(UUID.randomUUID(), "abcdef", userId));
        balanceRepository.save(BalanceEntity.builder()
                .id(UUID.randomUUID())
                .userId(userId)
                .value(new BigDecimal(400))
                .build());

        // when
        mockMvc.perform(MockMvcRequestBuilders.post("/balance/user/".concat(userId.toString().concat("/decrease")))
                .content("{\"value\": 1500, \"token\": \"abcdef\"}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();
    }

}
