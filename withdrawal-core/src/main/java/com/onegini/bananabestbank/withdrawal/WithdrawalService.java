package com.onegini.bananabestbank.withdrawal;

import com.onegini.bananabestbank.transaction.common.BalanceUpdater;
import com.onegini.bananabestbank.transaction.common.UpdatableBalance;
import com.onegini.bananabestbank.transaction.common.UserNotFoundException;
import com.onegini.bananabestbank.transaction.common.UserProvider;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

public class WithdrawalService {

    private final WithdrawnStorage withdrawnStorage;
    private final BalanceUpdater balanceUpdater;
    private final BalanceChecker balanceChecker;
    private final UserProvider userProvider;
    private final TokenValidator tokenValidator;

    public WithdrawalService(WithdrawnStorage withdrawnStorage, BalanceUpdater balanceUpdater,
                             BalanceChecker balanceChecker, UserProvider userProvider,
                             TokenValidator tokenValidator) {
        this.withdrawnStorage = withdrawnStorage;
        this.balanceUpdater = balanceUpdater;
        this.balanceChecker = balanceChecker;
        this.userProvider = userProvider;
        this.tokenValidator = tokenValidator;
    }

    public void makeWithdrawn(Withdrawn withdrawn, UUID userId) {
        validateUserExists(userId);
        validateToken(withdrawn.getToken(), userId);
        validateEnoughMoney(withdrawn.getValue(), userId);
        withdrawnStorage.applyWithdrawn(withdrawn, userId);
        updateBalance(withdrawn.getValue(), userId);
    }

    private void validateUserExists(UUID userId) {
        if (!userProvider.exists(userId)) {
            throw new UserNotFoundException("User with id ".concat(userId.toString()).concat(" was not found"));
        }
    }

    private void validateToken(String token, UUID userId) {
        if (!tokenValidator.isValid(token, userId)) {
            throw new TokenNotValidException("Token ".concat(token).concat(" is not valid for user ".concat(userId.toString())));
        }
    }

    private void validateEnoughMoney(BigDecimal value, UUID userId) {
        if (!balanceChecker.hasEnoughMoney(value, userId)) {
            throw new NotEnoughMoneyException("User ".concat(userId.toString()).concat(" has not enough money"));
        }
    }

    private void updateBalance(BigDecimal value, UUID userId) {
        balanceUpdater.update(decrease(value), userId);
    }

    private Consumer<UpdatableBalance> decrease(BigDecimal value) {
        return balance -> balance.setValue(balance.getValue().subtract(value));
    }

    public List<Withdrawn> getWithdrawnHistory(UUID userId) {
        return withdrawnStorage.getWithdrawnHistory(userId);
    }
}
