package com.onegini.bananabestbank.withdrawal;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class Withdrawn {

    private BigDecimal value;
    private String token;
    private LocalDateTime dateTime;
}
