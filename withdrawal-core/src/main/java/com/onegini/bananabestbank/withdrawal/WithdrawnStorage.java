package com.onegini.bananabestbank.withdrawal;

import java.util.List;
import java.util.UUID;

public interface WithdrawnStorage {
    void applyWithdrawn(Withdrawn withdrawn, UUID userId);

    List<Withdrawn> getWithdrawnHistory(UUID userId);
}
