package com.onegini.bananabestbank.withdrawal;

import java.math.BigDecimal;
import java.util.UUID;

public interface BalanceChecker {
    boolean hasEnoughMoney(BigDecimal value, UUID userId);
}
