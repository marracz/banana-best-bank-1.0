package com.onegini.bananabestbank.withdrawal;

import java.util.UUID;

public interface TokenValidator {

    boolean isValid(String token, UUID userId);
}
