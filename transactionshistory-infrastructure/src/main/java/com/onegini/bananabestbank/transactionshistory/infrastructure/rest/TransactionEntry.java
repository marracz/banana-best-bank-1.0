package com.onegini.bananabestbank.transactionshistory.infrastructure.rest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class TransactionEntry {

    private String type;
    private BigDecimal value;
}
