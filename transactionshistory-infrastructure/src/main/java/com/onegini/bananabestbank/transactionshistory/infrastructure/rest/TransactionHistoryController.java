package com.onegini.bananabestbank.transactionshistory.infrastructure.rest;

import com.onegini.bananabestbank.transactionshistory.Transaction;
import com.onegini.bananabestbank.transactionshistory.TransactionHistoryService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
public class TransactionHistoryController {

    private final TransactionHistoryService transactionHistoryService;

    public TransactionHistoryController(TransactionHistoryService transactionHistoryService) {
        this.transactionHistoryService = transactionHistoryService;
    }

    @GetMapping(value = "/history/user/{userId}", produces = APPLICATION_JSON_VALUE)
    public List<TransactionEntry> getTransactionHistory(@PathVariable("userId") UUID userId) {
        return transactionHistoryService.getTransactionHistory(userId).stream().map(this::toTransactionEntry).collect(Collectors.toList());
    }

    private TransactionEntry toTransactionEntry(Transaction transaction) {
        return new TransactionEntry(transaction.getType().toString().toLowerCase(), transaction.getValue());
    }
}
