package com.onegini.bananabestbank.transactionshistory.infrastructure.persistance;

import com.onegini.bananabestbank.transactionshistory.Transaction;
import com.onegini.bananabestbank.transactionshistory.TransactionStorageProvider;
import com.onegini.bananabestbank.transactionshistory.Type;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class JdbcTransactionStorage implements TransactionStorageProvider {

    private final EntityManager entityManager;

    @Autowired
    public JdbcTransactionStorage(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<Transaction> fetchHistory(UUID userId) {
        final Query query = entityManager.createNativeQuery("SELECT d.id, d.value, d.date_time, 'INCREASE' as type FROM deposit_entity d UNION ALL " +
                "SELECT w.id, w.value, w.date_time, 'DECREASE' as type FROM withdrawn_entity w ORDER BY date_time", TransactionEntity.class);

        return (List<Transaction>) query.getResultStream().map(rs -> {
            TransactionEntity te = (TransactionEntity) rs;
            return new Transaction(Type.valueOf(te.getType()), te.getValue().setScale(2), te.getDateTime());
        }).collect(Collectors.toList());
    }

    @Transactional
    @Override
    public void deleteAll() {
        entityManager.createNativeQuery("DELETE FROM deposit_entity").executeUpdate();
        entityManager.createNativeQuery("DELETE FROM withdrawn_entity").executeUpdate();
    }
}
