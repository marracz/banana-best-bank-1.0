package com.onegini.bananabestbank.transactionshistory.infrastructure;

import com.onegini.bananabestbank.transactionshistory.TransactionHistoryService;
import com.onegini.bananabestbank.transactionshistory.TransactionStorageProvider;
import com.onegini.bananabestbank.transactionshistory.infrastructure.persistance.JdbcTransactionStorage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManager;

@Configuration
public class TransactionHistoryConfiguration {

    @Bean
    public TransactionHistoryService transactionHistoryService(TransactionStorageProvider transactionStorageProvider) {
        return new TransactionHistoryService(transactionStorageProvider);
    }

    @Bean
    public TransactionStorageProvider transactionStorage(EntityManager entityManager) {
        return new JdbcTransactionStorage(entityManager);
    }
}
