package com.onegini.bananabestbank.transactionshistory.infrastructure.persistance;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Data
public class TransactionEntity {
    @Id
    private UUID id;
    private String type;
    private BigDecimal value;
    private LocalDateTime dateTime;
}
