package com.onegini.bananabestbank.transaction.common;

import java.math.BigDecimal;

public interface UpdatableBalance {

    BigDecimal getValue();

    void setValue(BigDecimal value);
}
