package com.onegini.bananabestbank.transaction.common;

import java.util.UUID;
import java.util.function.Consumer;

public interface BalanceUpdater {

    void update(Consumer<UpdatableBalance> operation, UUID userId);
}
