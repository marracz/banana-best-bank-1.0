package com.onegini.bananabestbank.transaction.common;

import java.util.UUID;

public interface UserProvider {

    boolean exists(UUID userId);
}
