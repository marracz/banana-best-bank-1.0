package com.onegini.bananabestbank.user.infrastructure;

import com.onegini.bananabestbank.user.UserService;
import com.onegini.bananabestbank.user.UserStorage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UserConfiguration {

    @Bean
    public UserStorage userStorage(UserRepository userRepository) {
        return new JdbcUserStorage(userRepository);
    }

    @Bean
    public UserService userService(UserStorage userStorage) {
        return new UserService(userStorage);
    }
}
