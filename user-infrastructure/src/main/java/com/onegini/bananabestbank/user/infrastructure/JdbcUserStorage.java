package com.onegini.bananabestbank.user.infrastructure;

import com.onegini.bananabestbank.user.User;
import com.onegini.bananabestbank.user.UserStorage;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

@Transactional
public class JdbcUserStorage implements UserStorage {

    private final UserRepository userRepository;

    public JdbcUserStorage(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Optional<User> findById(UUID id) {
        return userRepository.findById(id).map(user -> new User(user.getId()));
    }
}
