package com.onegini.bananabestbank.deposit.infrastructure;

import com.onegini.bananabestbank.deposit.DepositService;
import com.onegini.bananabestbank.deposit.DepositStorage;
import com.onegini.bananabestbank.deposit.infrastructure.persistance.DepositRepository;
import com.onegini.bananabestbank.deposit.infrastructure.persistance.JdbcDepositStorage;
import com.onegini.bananabestbank.transaction.common.BalanceUpdater;
import com.onegini.bananabestbank.transaction.common.UserProvider;
import com.onegini.bananabestbank.user.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DepositConfiguration {

    @Bean
    DepositStorage jdbcDepositStorage(DepositRepository depositRepository) {
        return new JdbcDepositStorage(depositRepository);
    }

    @Bean
    DepositService depositService(DepositStorage depositStorage, BalanceUpdater balanceUpdater, UserProvider userProvider) {
        return new DepositService(depositStorage, balanceUpdater, userProvider);
    }

    @Bean
    UserProvider userProvider(UserService userService) {
        return new UserProxy(userService);
    }
}
