package com.onegini.bananabestbank.deposit.infrastructure.persistance;

import com.onegini.bananabestbank.deposit.Deposit;
import com.onegini.bananabestbank.deposit.DepositStorage;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Transactional
public class JdbcDepositStorage implements DepositStorage {

    private final DepositRepository depositRepository;

    public JdbcDepositStorage(DepositRepository depositRepository) {
        this.depositRepository = depositRepository;
    }

    @Override
    public void applyDeposit(Deposit deposit, UUID userId) {
        depositRepository.save(new DepositEntity(UUID.randomUUID(), deposit.getValue(), userId, deposit.getDateTime()));
    }

    @Override
    public List<Deposit> getDepositHistory(UUID userId) {
        return depositRepository.findByUserId(userId)
                .map(depositEntity -> new Deposit(depositEntity.getValue(), depositEntity.getDateTime()))
                .collect(Collectors.toList());
    }
}