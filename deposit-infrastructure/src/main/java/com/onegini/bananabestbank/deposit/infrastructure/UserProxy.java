package com.onegini.bananabestbank.deposit.infrastructure;

import com.onegini.bananabestbank.transaction.common.UserProvider;
import com.onegini.bananabestbank.user.UserService;

import java.util.UUID;

public class UserProxy implements UserProvider {

    private final UserService userService;

    public UserProxy(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean exists(UUID userId) {
        return userService.findById(userId).isPresent();
    }
}
