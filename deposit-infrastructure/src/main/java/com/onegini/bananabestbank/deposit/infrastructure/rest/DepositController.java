package com.onegini.bananabestbank.deposit.infrastructure.rest;

import com.onegini.bananabestbank.deposit.Deposit;
import com.onegini.bananabestbank.deposit.DepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
public class DepositController {

    private final DepositService depositService;

    @Autowired
    public DepositController(DepositService depositService) {
        this.depositService = depositService;
    }

    @PostMapping(value = "/balance/user/{userId}/increase", consumes = APPLICATION_JSON_VALUE)
    public void deposit(@RequestBody DepositRequest depositRequest, @PathVariable("userId") UUID userId) {
        depositService.makeDeposit(new Deposit(depositRequest.getValue(), LocalDateTime.now()), userId);
    }
}
