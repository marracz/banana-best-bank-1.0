package com.onegini.bananabestbank.deposit.infrastructure.persistance;

import org.springframework.data.repository.CrudRepository;

import java.util.UUID;
import java.util.stream.Stream;

public interface DepositRepository extends CrudRepository<DepositEntity, UUID> {
    Stream<DepositEntity> findByUserId(UUID userId);
}