package com.onegini.bananabestbank.deposit.infrastructure.rest;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class DepositRequest {

    private BigDecimal value;
}
