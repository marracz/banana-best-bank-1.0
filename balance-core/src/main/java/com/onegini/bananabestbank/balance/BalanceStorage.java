package com.onegini.bananabestbank.balance;

import java.util.UUID;

public interface BalanceStorage {

    Balance fetchBalance(UUID userId);
}
