package com.onegini.bananabestbank.balance;

import java.util.UUID;

public class BalanceReaderService implements BalanceReader {

    private final BalanceStorage balanceStorage;

    public BalanceReaderService(BalanceStorage balanceStorage) {
        this.balanceStorage = balanceStorage;
    }

    @Override
    public Balance read(UUID userId) {
        return balanceStorage.fetchBalance(userId);
    }
}
