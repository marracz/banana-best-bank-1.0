package com.onegini.bananabestbank.balance;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@AllArgsConstructor
@Data
public class Balance {

    private BigDecimal value;

}
