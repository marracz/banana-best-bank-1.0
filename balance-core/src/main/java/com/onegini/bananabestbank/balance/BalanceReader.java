package com.onegini.bananabestbank.balance;

import java.util.UUID;

public interface BalanceReader {

    Balance read(UUID userId);
}
